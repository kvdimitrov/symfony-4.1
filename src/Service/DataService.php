<?php

namespace App\Service;

use App\Entity\Data;
use App\Entity\Users;

class DataService
{
    public function check(Users $user, Data $data=null, $courseCount)
    {
        $success = false;
        $counter = 0;

        if (count($courseCount)) {
            foreach ($courseCount as $counted) {
                $counter += $counted->getView();
            }
        }
        if ($data) {
            $start_date = $data->getReadDate();
            $since_start = $start_date->diff(new \DateTime('today'));
            $days = $since_start->days;
            if ($days != 0) {
                $success = true;
            } elseif ($days == 0 && $counter < 10) {
                $success = true;
            }
        }
        if ($user->getRole() == 1) {
            $success = true;
            $counter = 0;
        }
        return $result = ['view' => $success, 'counter' => $counter];
    }

    public function newRecord(Users $user, Data $data){

    }
}