<?php

namespace App\Controller;

use App\Entity\Courses;
use App\Entity\Data;

use App\Entity\Users;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CoursesController
 * @package App\Controller
 * @Route("/courses")
 */
class CoursesController extends Controller
{
    /**
     * @Route("/", name="courses_index")
     */
    public function index()
    {
        if (!$user = $this->get('session')->get('user')) {
            return $this->redirect($this->generateUrl('index'));
        }
        $courses = $this->getDoctrine()->getRepository(Courses::class)->findAll();
        return $this->render('courses/index.html.twig', ['courses' => $courses
        ]);
    }

    /**
     * @Route("/view", name="courses_view")
     */
    public function view(Request $request)
    {
        if (!$user = $this->get('session')->get('user')) {
            return $this->redirect($this->generateUrl('index'));
        }
        $course = $this->getDoctrine()->getRepository(Courses::class)->find($request->get('id'));
        $user = $this->getDoctrine()->getRepository(Users::class)->find($user->getId());
        $data = $this->getDoctrine()->getRepository(Data::class)->save($course, $user);
        return $this->render('courses/view.html.twig', ['course' => $course,'data' => $data
        ]);
    }

}
