<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\UsersLoginType;
use App\Form\UsersRegisterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class IndexController
 * @package App\Controller
 * @Route("/")
 */
class IndexController extends Controller
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request)
    {

        return $this->render('index/index.html.twig',
            []);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request)
    {
        if ($user = $this->get('session')->get('user')) {
            return $this->redirect($this->generateUrl('courses_index'));
        }
        $user = new Users();
        $userRepository = $this->getDoctrine()->getRepository(Users::class);
        $form = $this->createForm(UsersLoginType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if($userRepository->loginUser($user)){
                    return $this->redirect($this->generateUrl('courses_index'));
                }
            }
        }
        return $this->render('index/login.html.twig',
            ['form' => $form->createView()]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request)
    {
        if ($user = $this->get('session')->get('user')) {
            return $this->redirect($this->generateUrl('courses_index'));
        }
        $user = new Users();
        $userRepository = $this->getDoctrine()->getRepository(Users::class);
        $form = $this->createForm(UsersRegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if($userRepository->registerUser($user, $form['repeat_password']->getData())){
                    return $this->redirect($this->generateUrl('courses_index'));
                }
            }
        }
        return $this->render('index/register.html.twig',
            ['form' => $form->createView()]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        session_destroy();
        return $this->redirect($this->generateUrl('index'));
    }
}
