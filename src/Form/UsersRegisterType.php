<?php
namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;

class UsersRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('label' => 'Username', 'required' => true, 'constraints' => new NotBlank()))
            ->add('email', EmailType::class, array('label' => 'Enter your email', 'required' => true,'constraints' => new NotBlank()))
            ->add('password', PasswordType::class, array('label' => 'Enter your password', 'required' => true, 'constraints' => new NotBlank()))
            ->add('repeat_password', PasswordType::class, array('label' => 'Re-enter your password', 'required' => true, 'constraints' => new NotBlank(), 'mapped' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Users::class,
        ));
    }
}