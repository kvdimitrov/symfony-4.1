<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursesRepository")
 */
class Courses
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $youtube;

    /**
     * @ORM\OneToMany(targetEntity="Data", mappedBy="course", cascade={"persist", "remove"})
     */
    private $data;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param mixed $youtube
     * @return $this
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
        return $this;
    }

    /**
     * @param Data $data
     * @return Courses
     */
    public function addData(Data $data)
    {
        $this->data[] = $data;

        return $this;
    }

    /**
     * @param Data $data
     */
    public function removeData(Data $data)
    {
        $this->data->removeElement($data);
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        return $this->data;
    }
}