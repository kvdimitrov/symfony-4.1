<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DataRepository")
 */
class Data
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime")
     */
    private $readDate;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="data")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Courses", inversedBy="data")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $course;

    /**
     * @ORM\Column(type="integer")
     */
    private $view;


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getReadDate()
    {
        return $this->readDate;
    }

    /**
     * @param mixed $readDate
     * @return $this
     */
    public function setReadDate($readDate)
    {
        $this->readDate = $readDate;
        return $this;
    }

    /**
     * @param Users $user
     * @return $this
     */
    public function setUser(Users $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Users
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param Courses $course
     * @return $this
     */
    public function setCourse(Courses $course = null) {
        $this->course = $course;

        return $this;
    }

    /**
     * @return Courses
     */
    public function getCourse() {
        return $this->course;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param mixed $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }

}