<?php
namespace App\DataFixtures;

use App\Entity\Courses;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

CONST password = "admin";

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $course = new Courses();
            $course->setName("Youtube ".$i);
            $course->setYoutube("<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/sT4dMKSA1-A\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>");
            $manager->persist($course);

            $user = new Users();
            $user->setEmail("test" . $i . "@abv.bg");
            $user->setUsername("test" . $i);
            $user->setPassword(password_hash(password, PASSWORD_BCRYPT));
            if ($i == 0) {
                $user->setRole(1);
            } else {
                $user->setRole(2);
            }
            $manager->persist($user);
        }
        $manager->flush();
    }
}