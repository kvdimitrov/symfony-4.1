Symfony Standard Edition
========================

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration


Installation
========================
* Set vhost


        <VirtualHost *:80>
          ServerName project.prod
          ServerAlias www.project.prod
      
          DocumentRoot /symfony/install/dir/public
          <Directory /symfony/install/dir/public>
              AllowOverride None
              Require all granted
              Allow from All
      
              <IfModule mod_rewrite.c>
                  Options -MultiViews
                  RewriteEngine On
                  RewriteCond %{REQUEST_FILENAME} !-f
                  RewriteRule ^(.*)$ index.php [QSA,L]
              </IfModule>
          </Directory>
      
          # uncomment the following lines if you install assets as symlinks
          # or run into problems when compiling LESS/Sass/CoffeeScript assets
          # <Directory /symfony/install/dir/>
          #     Options FollowSymlinks
          # </Directory>
      
          # optionally disable the RewriteEngine for the asset directories
          # which will allow apache to simply reply with a 404 when files are
          # not found instead of passing the request into the full symfony stack
          <Directory /symfony/install/dir/public/bundles>
              <IfModule mod_rewrite.c>
                  RewriteEngine Off
              </IfModule>
          </Directory>
          ErrorLog /var/log/apache2/project.prod_error.log
          CustomLog /var/log/apache2/project.prod_access.log combined
      
          # optionally set the value of the environment variables used in the application
          #SetEnv APP_ENV prod
          #SetEnv APP_SECRET <app-secret-id>
          #SetEnv DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name"
      </VirtualHost>

* Set hosts
        
        go and edit /etc/hosts file add: 127.0.0.1 project.prod
        
* Create database
* Run composer install
* Add database credentials into symfony root dir .env file
* Run in console: php bin/console d:migration:migrate
* Run in console: php bin/console doctrine:fixtures:load
* Restart apache2 server

        sudo service apache2 restart

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.2/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.2/doctrine.html
[8]:  https://symfony.com/doc/3.2/templating.html
[9]:  https://symfony.com/doc/3.2/security.html
[10]: https://symfony.com/doc/3.2/email.html
[11]: https://symfony.com/doc/3.2/logging.html
[12]: https://symfony.com/doc/3.2/assetic/asset_management.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
